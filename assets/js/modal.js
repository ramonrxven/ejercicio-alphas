// objeto modal
var modal = document.getElementById("myModal");

//objeto que contiene el video
var iframeObj = document.getElementById("video");

// url origen del video
var videoSrc = "https://www.youtube.com/embed/yuTqPtcAjbc";

// objeto boton que abre el modal
var btn = document.getElementById("btn-video-modal");

// objeto boton que cierra el modal
var span = document.getElementsByClassName("close-video-modal")[0];

// Mostrar el modal
btn.onclick = function(e) {
  e.preventDefault();
  modal.style.display = "block";
  iframeObj.setAttribute('src',videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
}

// Cuando se haga clic en la (X) Cerrar el modal
span.onclick = function(e) {
  e.preventDefault();
  closeModal();
}

// Cerrar el modal cuando se haga clic fuera del mismo
window.onclick = function(event) {
  if (event.target == modal) {
    closeModal();
  }
}

// cierra  la ventana modal
// desacopla el video del iframe para que se detenga la reproducción.
function closeModal()
{
  iframeObj.setAttribute('src', " " ); 
  modal.style.display = "none";

}
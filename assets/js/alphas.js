document.documentElement.className = 'js'
window.dataLayer = window.dataLayer || [];

var et_site_url = 'https://alphas.technology';
var et_post_id = '17';
var Tawk_API = Tawk_API || {};
var Tawk_LoadStart = new Date();

var et_core_api_spam_recaptcha = {
    "site_key": "6Le20OwUAAAAAIIECDOV3UTU4P6-MM0WCevU2lvd",
    "page_action": {
        "action": "alphas_technology"
    }
}

function et_core_page_resource_fallback(a, b) {
  "undefined" === typeof b && (b = a.sheet.cssRules && 0 === a.sheet.cssRules.length);
  b && (a.onerror = null, a.onload = null, a.href ? a.href = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id : a.src && (a.src = et_site_url + "/?et_core_page_resource=" + a.id + et_post_id))
}

function loadAsync(e, t) {
    var a, n = !1;
    a = document.createElement("script"), a.type = "text/javascript", a.src = e, a.onreadystatechange = function() {
      n || this.readyState && "complete" != this.readyState || (n = !0, "function" == typeof t && t())
    }, a.onload = a.onreadystatechange, document.getElementsByTagName("head")[0].appendChild(a)
  }

jQuery(function($) {
    jQuery('.button').click(function() {
    jQuery('.popup').css('display', 'block')
    });
    jQuery('.close').click(function() {
    jQuery('.popup').css('display', 'none')
    })
});

    
function gtag() {
  dataLayer.push(arguments)
}

gtag('js', new Date());
gtag('config', 'UA-153076699-1')


var et_animation_data = [{
  "class": "et_pb_section_0",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out",
  "duration_phone": "0ms"
}, {
  "class": "et_pb_row_0",
  "style": "fade",
  "repeat": "once",
  "duration": "0ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_1",
  "style": "fade",
  "repeat": "once",
  "duration": "450ms",
  "delay": "100ms",
  "intensity": "50%",
  "starting_opacity": "9%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_0",
  "style": "slideBottom",
  "repeat": "once",
  "duration": "1000ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_1",
  "style": "slideBottom",
  "repeat": "once",
  "duration": "1000ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_2",
  "style": "slideBottom",
  "repeat": "once",
  "duration": "1000ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_3",
  "style": "slideBottom",
  "repeat": "once",
  "duration": "1000ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_4",
  "style": "slideBottom",
  "repeat": "once",
  "duration": "1000ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_2",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_3",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_5",
  "style": "foldBottom",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_6",
  "style": "foldBottom",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_image_7",
  "style": "foldBottom",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_4",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_6",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_7",
  "style": "fade",
  "repeat": "once",
  "duration": "350ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_fullwidth_code_0",
  "style": "slideRight",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "33%",
  "starting_opacity": "10%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_section_10",
  "style": "flip",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}, {
  "class": "et_pb_row_20",
  "style": "slide",
  "repeat": "once",
  "duration": "450ms",
  "delay": "0ms",
  "intensity": "50%",
  "starting_opacity": "0%",
  "speed_curve": "ease-in-out"
}]

var DIVI = {
  "item_count": "%d Item",
  "items_count": "%d Items"
};
var et_frontend_scripts = {
  "builderCssContainerPrefix": "#et-boc",
  "builderCssLayoutPrefix": "#et-boc .et-l"
};
var et_pb_custom = {
  "ajaxurl": "https:\/\/alphas.technology\/wp-admin\/admin-ajax.php",
  "images_uri": "https:\/\/alphas.technology\/wp-content\/themes\/Divi\/images",
  "builder_images_uri": "https:\/\/alphas.technology\/wp-content\/themes\/Divi\/includes\/builder\/images",
  "et_frontend_nonce": "b268ed4567",
  "subscription_failed": "Por favor, revise los campos a continuaci\u00f3n para asegurarse de que la informaci\u00f3n introducida es correcta.",
  "et_ab_log_nonce": "6ce3dd187f",
  "fill_message": "Por favor, rellene los siguientes campos:",
  "contact_error_message": "Por favor, arregle los siguientes errores:",
  "invalid": "De correo electr\u00f3nico no v\u00e1lida",
  "captcha": "Captcha",
  "prev": "Anterior",
  "previous": "Anterior",
  "next": "Siguiente",
  "wrong_captcha": "Ha introducido un n\u00famero equivocado de captcha.",
  "wrong_checkbox": "Checkbox",
  "ignore_waypoints": "no",
  "is_divi_theme_used": "1",
  "widget_search_selector": ".widget_search",
  "ab_tests": [],
  "is_ab_testing_active": "",
  "page_id": "17",
  "unique_test_id": "",
  "ab_bounce_rate": "5",
  "is_cache_plugin_active": "no",
  "is_shortcode_tracking": "",
  "tinymce_uri": ""
};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {
  "desktop": [],
  "tablet": [],
  "phone": []
}
